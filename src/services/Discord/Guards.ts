import { DMChannel, GroupDMChannel, TextChannel, VoiceChannel, CategoryChannel, NewsChannel, StoreChannel } from 'discord.js';

export const isDmChannel = (channel): channel is DMChannel => channel.type === 'dm';
export const isGroupChannel = (channel): channel is GroupDMChannel => channel.type === 'group';
export const isTextChannel = (channel): channel is TextChannel => channel.type === 'text';
export const isVoiceChannel = (channel): channel is VoiceChannel => channel.type === 'voice';
export const isCategoryChannel = (channel): channel is CategoryChannel => channel.type === 'category';
export const isNewsChannel = (channel): channel is NewsChannel => channel.type === 'news';
export const isStoreChannel = (channel): channel is StoreChannel => channel.type === 'store';