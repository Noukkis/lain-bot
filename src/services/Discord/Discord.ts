import { basename } from 'path';
import { Client, FileOptions } from 'discord.js';
import { isTextChannel, isDmChannel, isGroupChannel } from './Guards';
import logger from '@/logger';

export default class Discord {

  private readonly client: Client
  private readonly token: string

  public constructor(token: string) {
    this.token = token;
    this.client = new Client();
    this.client.on('error', error => logger.error(error));
    this.client.on('debug', debug => logger.debug(debug));
    this.client.on('warn', warn => logger.warn(warn));
  }

  public async init(): Promise<void> {
    this.client.login(this.token);
    return new Promise(resolve => this.client.on('ready', resolve));
  }

  public async sendMessage(channelId: string, text: string, filesArray?: string[]): Promise<void> {
    const channel = this.client.channels.get(channelId);
    if (isTextChannel(channel) || isDmChannel(channel) || isGroupChannel(channel)) {
      const files = filesArray ? this.getFiles(filesArray) : null;
      if(files) await channel.send(text, { files });
      else await channel.send(text);
    } else {
      throw new Error(`${channelId} is not a text channel`);
    }
  }

  private getFiles(files: string[]): FileOptions[] {
    return files.map(attachment => {
      const name = basename(attachment);
      return { name, attachment };
    });
  }

}