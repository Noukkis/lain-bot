import pug, { compileTemplate } from 'pug';
import sass from 'sass';
import puppeteer from 'puppeteer';
import { promisify } from 'util';
import { Restaurant } from '@/services/MenusManager/Restaurant';


const renderSass = promisify(sass.render);

export default class Restaurant2img {


  private readonly sassFile: string;
  private readonly pugFile: string;
  private readonly outputFile: string;

  private style: string;
  private template: compileTemplate;

  public constructor() {
    this.sassFile = './templates/styles.scss';
    this.pugFile = './templates/menuTable.pug';
    this.outputFile = 'table.png';
  }

  public async init(): Promise<void> {
    const { css } = await renderSass({ file: this.sassFile, outputStyle: 'compressed' });
    this.style = css.toString();
    this.template = pug.compileFile(this.pugFile);
  }

  public async transform(restaurants: Restaurant[]): Promise<string> {
    const html = this.transformHtml(restaurants);
    const file = await this.screenshot(html, this.outputFile, '.content', 5);
    return file;
  }

  private transformHtml(restaurants: Restaurant[]): string {
    const style = this.style;
    const html = this.template({ restaurants, style });
    return html;
  }

  private async screenshot(html: string, file: string, selector: string, padding: number = 0): Promise<string> {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();  
    await page.setViewport({ width: 1920, height: 1200, deviceScaleFactor: 1 });         
    await page.setContent(html);
    const rect = await page.evaluate(selector => {
      const element = document.querySelector(selector);
      const {x, y, width, height} = element.getBoundingClientRect();
      return {left: x, top: y, width, height, id: element.id};
    }, selector);
    await page.screenshot({
      path: file,
      clip: {
        x: rect.left - padding,
        y: rect.top - padding,
        width: rect.width + padding * 2,
        height: rect.height + padding * 2
      }
    });
    await browser.close();
    return file;
  }

}