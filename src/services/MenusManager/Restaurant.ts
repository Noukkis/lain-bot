export interface Restaurant {
  name: string;
  url: string;
  menus: Menu[];
}

export interface Menu {
  price: number;
  dishes: string[];
  infos?: string;
  isVegetarian?: boolean;
}

export interface RestaurantGetter {
  name: string;
  url: string;
  getMenus: () => Promise<Menu[]>;
}