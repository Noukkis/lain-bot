import Heia from './Heia';
import MensaPerolles from './MensaPerolles';
import { RestaurantGetter } from '../Restaurant';

const HEIA_URL = 'https://heia-fr.sv-restaurant.ch/fr/plan-des-menus/';
const MOZAIK_URL = 'https://mozaik-fr.sv-restaurant.ch/fr/plan-des-menus/';
const UNIFR_URL = 'https://www3.unifr.ch/mensa/fr/';

const restaurantGetters: RestaurantGetter[] = [
  new MensaPerolles('Mensa Pérolles', UNIFR_URL),
  new Heia('HEIA-FR', HEIA_URL),
  new Heia('Mozaik', MOZAIK_URL),
];

export default restaurantGetters;