import { JSDOM } from 'jsdom';
import axios from 'axios';
import { RestaurantGetter, Menu } from '../Restaurant';

export default class MensaPerolles implements RestaurantGetter {
  
  public readonly url: string;
  public readonly name: string;

  public constructor(name: string, url: string) {
    this.name = name;
    this.url = url;
  }
  
  public async getMenus(): Promise<Menu[]> {
    const menus: Menu[] = [];
    const xMenus = await this.getMenusXml();
    xMenus.forEach(({ children }) => {
      const price = this.parsePrice(children);
      const mainDish = this.parseMainDish(children);
      const sideDishes = this.parseSideDishes(children);
      const dishes = mainDish.concat(sideDishes);
      const { infos, isVegetarian } = this.parseInfos(children);
      menus.push({ price, dishes, infos, isVegetarian });
    });
    return menus;
  }

  private async getMenusXml(): Promise<NodeListOf<Element>> {
    const result = await axios.get(this.url);
    const { window: { document } } = new JSDOM(result.data);
    const xMenusContainer = document.querySelector('.row div[data-accordion-state=open] .col-md-4:nth-child(2)');
    const xMenus = xMenusContainer.querySelectorAll('.menu-item');
    return xMenus;
  }

  private parsePrice(children: HTMLCollection): number {
    const priceNode = children.item(0).querySelector('small');
    const priceText = priceNode.innerHTML;
    return parseFloat(priceText);
  }

  private parseMainDish(children: HTMLCollection): string[] {
    const raw = children.item(1).innerHTML;
    const dishes = raw.split('<br>');
    dishes[1] = dishes[1].replace(/<\/?small>/g, '');
    if(!dishes[1]) dishes.pop();
    return dishes;
  }

  private parseSideDishes(children: HTMLCollection): string[] {
    const dish = children.item(2).innerHTML;
    const dishes = dish.split('<br>\n');
    return dishes;
  }

  private parseInfos(children: HTMLCollection): { infos?: string; isVegetarian?: boolean } {
    const xInfos = children.item(3);
    if(!xInfos) return {};
    let infos = xInfos.textContent;
    if(infos.trim() == 'Menu végétarien') infos = null;
    const isVegetarian = xInfos.innerHTML.includes('veg.png');
    return { infos, isVegetarian };
  }

}