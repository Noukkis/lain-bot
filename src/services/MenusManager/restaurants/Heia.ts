import { JSDOM } from 'jsdom';
import axios from 'axios';
import { RestaurantGetter, Menu } from '../Restaurant';

export default class Heia implements RestaurantGetter {
  
  public readonly url: string;
  public readonly name: string;

  public constructor(name: string, url: string) {
    this.name = name;
    this.url = url;
  }
  
  public async getMenus(): Promise<Menu[]> {
    const menus: Menu[] = [];
    const xMenus = await this.getMenusXml();
    xMenus.forEach(({ children }) => {
      const xMenu = children.item(0);
      const price = this.parsePrice(xMenu);
      const mainDish = this.parseMainDish(xMenu);
      const sideDishes = this.parseSideDishes(xMenu);
      const dishes = mainDish.concat(sideDishes);
      const { infos, isVegetarian } = this.parseInfos(xMenu);
      menus.push({ price, dishes, infos, isVegetarian });
    });
    return menus;
  }

  private async getMenusXml(): Promise<NodeListOf<Element>> {
    const result = await axios.get(this.url);
    const { window: { document } } = new JSDOM(result.data);
    const xMenus = document.querySelectorAll('#menu-plan-tab1 .menu-item');
    return xMenus;
  }

  private parsePrice(xMenu: Element): number {
    const priceNode = xMenu.querySelector('.price .val');
    const priceText = priceNode.textContent;
    return parseFloat(priceText);
  }

  private parseMainDish(xMenu: Element): string[] {
    const dishNode = xMenu.querySelector('.menu-title');
    return [ dishNode.textContent ];
  }

  private parseSideDishes(xMenu: Element): string[] {
    const dishNode = xMenu.querySelector('.menu-description');
    const dish = dishNode.innerHTML;
    const dishes = dish.split('<br>\n');
    return dishes;
  }

  private parseInfos(xMenu: Element): { infos?: string; isVegetarian?: boolean } {
    const xInfos = xMenu.querySelector('.menu-provenance');
    const infos = xInfos.textContent;
    const isVegetarian = !!xMenu.querySelector('.label-vegetarian');
    return { infos, isVegetarian };
  }
}