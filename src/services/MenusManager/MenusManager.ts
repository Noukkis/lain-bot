import { Restaurant } from './Restaurant';
import restaurantGetters from './restaurants';

export default class MenusManager {

  public async getRestaurants(): Promise<Restaurant[]> {
    const pMenus = restaurantGetters.map(restaurant => restaurant.getMenus());
    const restaurants: Restaurant[] = [];
    for(let i = 0; i < pMenus.length; i++) {
      const { name, url } = restaurantGetters[i];
      const menus = await pMenus[i];
      restaurants.push({ name, url, menus });
    }
    return restaurants;
  }

}