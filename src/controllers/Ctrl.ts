import logger from '@/logger';
import { Config } from '@/config';
import Discord from '@/services/Discord';
import MenusCtrl from './MenusCtrl';

export default class Ctrl {

  private readonly config: Config;
  private readonly discord: Discord;
  private readonly menus: MenusCtrl;

  public constructor(config: Config) {
    this.config = config;
    this.discord = new Discord(this.config.token);
    this.menus = new MenusCtrl(this.config, this);
  }

  public async init(): Promise<void> {
    const pDiscord = this.discord.init();
    const pMenus = this.menus.init();
    await Promise.all([ pDiscord, pMenus ]);
    logger.info('Bot initialized and ready');
  }

  public start(): void {
    this.menus.start();
  }

  public sendMessage(channelId: string, text: string, filesArray?: string[]): Promise<void> {
    return this.discord.sendMessage(channelId, text, filesArray);
  }

}
