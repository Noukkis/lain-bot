import { CronJob } from 'cron';
import logger from '@/logger';
import { Config } from '@/config';
import MenusManager from '@/services/MenusManager';
import { trim } from '@/utils';
import Restaurant2img from '@/services/Restaurant2img';
import Ctrl from './Ctrl';
import { Restaurant } from '@/services/MenusManager/Restaurant';

export default class MenusCtrl {

  private readonly config: Config;
  private readonly ctrl: Ctrl;
  private readonly menus: MenusManager;
  private readonly restaurant2img: Restaurant2img;
  private readonly job: CronJob;

  public constructor(config: Config, ctrl: Ctrl) {
    this.config = config;
    this.ctrl = ctrl;
    this.menus = new MenusManager();
    this.restaurant2img = new Restaurant2img();
    this.job = new CronJob(this.config.cron, () => this.sendMenus());
  }

  public async init(): Promise<void> {
    await this.restaurant2img.init();
  }

  public start(): void {
    this.job.start();
  }

  private async sendMenus(): Promise<void> {
    logger.info('Retrieving menu');
    const restaurants = await this.menus.getRestaurants();
    this.trimMenus(restaurants);
    const file = await this.restaurant2img.transform(restaurants);
    this.ctrl.sendMessage(this.config.menuChannel, 'Menus du jour', [ file ]);
    logger.info('Message sent');
  }

  private trimMenus(restaurants: Restaurant[]): void {
    restaurants.forEach(restaurant => {
      restaurant.menus.forEach(menu => {
        menu.infos = trim(menu.infos);
        const dishes = [];
        menu.dishes.forEach(dish => {
          dish = trim(dish);
          if(dish) dishes.push(dish);
        });
        menu.dishes = dishes;
      });
    });
  }

}
