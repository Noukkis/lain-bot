import { mkdirSync, existsSync } from 'fs';
import { Logger, LoggerOptions, createLogger, transports, format } from 'winston';
import { Environment, getEnvironment } from '@/utils';

const { Console, File } = transports;
const { combine, timestamp, printf, splat } = format;

const logLevel = (getEnvironment() == Environment.Dev) ? 'debug' : 'info';

if(!existsSync('log')) {
  mkdirSync('log');
}

const logFormat = printf((info) => {
  return `[${info.timestamp}] ${info.level.toUpperCase()}: ${info.message}`;
});

const options: LoggerOptions = {
  level: logLevel,
  transports: [
    new Console(),
    new File({
      filename: 'log/combined.log',
      maxsize: 5242880,
      maxFiles: 5,
      tailable: true
    })
  ],
  format: combine(
    timestamp({format: 'YYYY-MM-DD HH:mm:ss'}),
    splat(),
    logFormat
  )
};

const logger: Logger = createLogger(options);

export default logger;
