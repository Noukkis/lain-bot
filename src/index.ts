import { join } from 'path';
import Ctrl from '@/controllers';
import logger from '@/logger';
import { Config, ConfigManager } from '@/config';
import { getEnvironment } from '@/utils';
import { displayName, version } from '../package.json';

const CONFIG_FILE = join(__dirname, '../config/config.yml');

init();

async function start(startFunc: (ctrl: Ctrl) => void): Promise<void> {
  logger.info(`Starting ${displayName} v${version} for ${getEnvironment()}`);
  const config = await loadConfig();
  const ctrl = createCtrl(config);
  await ctrl.init();
  startFunc(ctrl);
}

async function loadConfig(): Promise<Config> {
  const manager = new ConfigManager(CONFIG_FILE);
  await manager.loadConfig();
  return manager.config;
}

function createCtrl(config: Config): Ctrl {
  return new Ctrl(config);
}

function init(): void {
  start(ctrl => ctrl.start());
}