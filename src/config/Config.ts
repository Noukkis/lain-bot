export default class Config {

  public token: string;
  public menuChannel: string;
  public cron: string;

  public constructor() {
    this.token = '<Your token here>';
    this.menuChannel = '<Your menus channel ID here>';
    this.cron = '* * * * *';
  }

}

export interface Menu {
  name: string;
  link: string;
  selector: string;
}