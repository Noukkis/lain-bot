import yaml from 'js-yaml';
import Config from './Config';
import logger from '@/logger';
import { fileExists, readFile, writeFile } from '@/utils';

export default class ConfigManager {

  private readonly filePath: string;
  public readonly config: Config;

  public constructor(filePath) {
    this.filePath = filePath;
    this.config = new Config();
  }

  public async loadConfig(): Promise<void> {
    logger.info('loading config');
    const exists = await fileExists(this.filePath);
    if(exists) return this.loadConfigFile();
    return this.writeDefaultConfigFile();
  }
  
  private async loadConfigFile(): Promise<void> {
    const text = await readFile(this.filePath);
    const config = yaml.safeLoad(text);
    Object.assign(this.config, config);
    logger.info('Config loaded');
  }

  private async writeDefaultConfigFile(): Promise<void> {
    logger.info('Config not found: Creating default config file');
    const text = yaml.safeDump(this.config);
    await writeFile(this.filePath, text);
    logger.info('Config created');
  }

}
