import { join } from 'path';

export const root = join(__dirname, '..');

export const src = 'src';
export const log = join(root, 'log/');
export const dest = join(root, 'dist/');
export const tsconfig = join(root, 'tsconfig.json');