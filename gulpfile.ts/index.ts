import { task, parallel, series, src, dest } from 'gulp';
import * as Vars from './vars';
import { createProject } from 'gulp-typescript';
import nodemon from 'gulp-nodemon';
import del from 'del';

const tsProject = createProject(Vars.tsconfig);

task('gulp', () => {
  return src('gulpfile.ts/**/*.ts')
    .pipe(tsProject())
    .js.pipe(dest('gulpfile.js'));
});

task('build', () => {
  return src(Vars.src + '/**/*.ts')
    .pipe(tsProject())
    .js.pipe(dest(Vars.dest));
});

task('dev', (done) => {
  nodemon({
    watch: [Vars.src, 'config'],
    ext: 'ts pug scss',
    exec: 'NODE_ENV=development ts-node -r tsconfig-paths/register ' + Vars.src,
    done: done
  });
});

task('clean:log', async () => {
  await del(Vars.log);
});

task('clean:dist', async () => {
  await del(Vars.dest);
});

task('clean', parallel('clean:dist', 'clean:log'));
task('cleanAndBuild', series('clean', 'build'));