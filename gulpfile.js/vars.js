"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var path_1 = require("path");
exports.root = path_1.join(__dirname, '..');
exports.src = 'src';
exports.log = path_1.join(exports.root, 'log/');
exports.dest = path_1.join(exports.root, 'dist/');
exports.tsconfig = path_1.join(exports.root, 'tsconfig.json');
